Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PostScript
Upstream-Contact: Shawn Wallace
Source: https://metacpan.org/release/PostScript

Files: *
Copyright: Copyright 1998, 1999 Shawn Wallace. All rights reserved.
License: Artistic or GPL-1+
Comment:
  Document.pm and TextBlock.pm are missing any copyright statement: I've contacted
  the upstream author and he confirmed that these files are released under the
  same terms of the rest of the module (i.e. Perl-like GPL-1+ and Artistic).
  For further reference:
   <http://rt.cpan.org/Public/Bug/Display.html?id=32319>

Files: debian/*
Copyright: 2008, 2009, gregor herrmann <gregoa@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2007-2008, David Paleino <d.paleino@gmail.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
